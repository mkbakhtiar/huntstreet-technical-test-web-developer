<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/admin/login', 'AdminController@login');

Route::get('/admins', 'AdminController@index');
Route::post('/admin/store', 'AdminController@store');
Route::get('/admin/edit/{id}', 'AdminController@getAdmin');
Route::get('/admin/show/{id}', 'AdminController@getAdmin');
Route::put('/admin/update/{id}', 'AdminController@update');
Route::delete('/admin/delete/{id}', 'AdminController@delete'); 

Route::get('/userss', 'UsersUndanganController@index');
Route::post('/users/store', 'UsersUndanganController@store');

Route::get('/users/edit/{id}', 'UsersUndanganController@getUsers');
Route::get('/users/send-link/{id}', 'UsersUndanganController@sendLink');
Route::get('/users/show/{id}', 'UsersUndanganController@getUsers');
Route::put('/users/update/{id}', 'UsersUndanganController@update');
Route::delete('/users/delete/{id}', 'UsersUndanganController@delete'); 
Route::get('/getml/{id}', 'UsersUndanganController@getMl');
Route::get('/cekregistrasi/{id}', 'UsersUndanganController@cekR');
Route::get('/getRegistrasi/{id}', 'UsersUndanganController@getR');
Route::get('/get-designers/{id}', 'UsersUndanganController@getListDesigner');
Route::post('/event/daftar', 'UsersUndanganController@daftar');
Route::post('/event/setdesignerfav', 'UsersUndanganController@setDesigner');

Route::get('/event-designers', 'UsersUndanganController@designer');
Route::get('/designers', 'DesignerController@index');

Route::post('/designer/store', 'DesignerController@store');
Route::get('/designer/edit/{id}', 'DesignerController@getDesigner');
Route::get('/designer/foto/{id}', 'DesignerFotoController@index');
Route::put('/designer/update/{id}', 'DesignerController@update');
Route::delete('/designer/delete/{id}', 'DesignerController@delete'); 

Route::get('/designer-foto/{id}', 'DesignerFotoController@show');

Route::get('/get-designer-name/{id}', 'DesignerFotoController@designerName');
Route::delete('/designerfoto/delete/{id}', 'DesignerFotoController@delete');
Route::post('upload', 'DesignerFotoController@store'); 
// Route::post('/image/store', 'DesignerFotoController@store');


