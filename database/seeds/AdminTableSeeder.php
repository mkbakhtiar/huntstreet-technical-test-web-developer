<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class AdminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement("truncate table tb_admin");
        DB::table('tb_admin')->insert([
            'nama' => 'Admin',
            'username' => 'admin@gmail.com',
            'password' => Hash::make('admin'),
            'created_at' => date('Y-m-d H:i:s'),
        ]);
    }
}
