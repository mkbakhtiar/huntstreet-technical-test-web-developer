<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbUndanganUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_undangan_user', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->char('nama', 75)->nullable();
            $table->date('tgl_lahir')->nullable();
            $table->char('jk',2)->nullable();
            $table->char('email',35);
            $table->char('link',150)->nullable();
            $table->integer('input_by')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_undangan_user');
    }
}
