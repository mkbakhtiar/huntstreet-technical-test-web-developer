<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Designer extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nama'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $table = "tb_designer";
}
