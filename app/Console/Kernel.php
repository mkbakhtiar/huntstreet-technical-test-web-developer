<?php

namespace App\Console;
use DB;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function () {            
            $get = DB::table('tb_undangan')->join('tb_undangan_user','tb_undangan.id_user','=','tb_undangan_user.id')->where('tb_undangan.is_sendmail', '<>', 1)->get();
            foreach($get as $getA){
                $tgl1 = new DateTime($getA->created_at);
                $tgl2 = new Date("Y-m-d H:i:s");
                $jarak = $tgl2->diff($tgl1);

                $jarakjam = $jarak->h;
                if($jarakjam >= 1){
                    $to_name = $getA->nama;
                    $to_email = $getA->email;
                    $data = array('name'=>$user->email, "body" => "Halo, Terimakasih telah mendaftar Event HuntBazaar ".$getA->kode_reg);
                    
                    Mail::send('emails.mail_trims', $data, function($message) use ($to_name, $to_email) {
                        $message->to($to_email, $to_name)
                        ->subject('Terimakasih Telah Mendaftar Event HUNTBAZAAR');
                        $message->from('mkbakhtiar.new@gmail.com','Terimakasih Telah Mendaftar Event HUNTBAZAAR');
                    });

                    $update = DB::table('tb_undangan')->where('kode_reg', $getA->kode_reg)->update([
                        'is_sendmail' => 1
                    ]);
                }
            }
        })->hourly();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
