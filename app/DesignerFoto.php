<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DesignerFoto extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_designer','image'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $table = "tb_designer_img";
}
