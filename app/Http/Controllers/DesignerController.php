<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;

class DesignerController extends Controller
{
    public function index(){
        $getData = DB::table('tb_designer')->get();
        return $getData->toJson();
    }
    public function store(Request $request)
    {
        $validatedData = $request->validate([
          'nama' => 'required',
        ]);
 
        $project = DB::table('tb_designer')->insert(
            ['nama' => $request->nama]
        );
        
        $msg = [
            'success' => true,
            'message' => 'Designer created successfully!'
        ];
 
        return response()->json($msg);
    }
    public function getDesigner($id) // for edit and show
    {
        $article = \App\Designer::find($id);
 
        return $article->toJson();
    }
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
          'nama' => 'required',
        ]);
 
        $article = \App\Designer::find($id);
        $article->nama = $validatedData['nama'];
        $article->save();
 
        $msg = [
            'success' => true,
            'message' => 'Designer updated successfully'
        ];
 
        return response()->json($msg);
    }
    public function delete($id)
    {
        $designer = \App\Designer::find($id);
        if(!empty($designer)){
            $designer->delete();
            $msg = [
                'success' => true,
                'message' => 'Designer deleted successfully!'
            ];
            return response()->json($msg);
        } else {
            $msg = [
                'success' => false,
                'message' => 'Designer deleted failed!'
            ];
            return response()->json($msg);
        }
    }
    
}
