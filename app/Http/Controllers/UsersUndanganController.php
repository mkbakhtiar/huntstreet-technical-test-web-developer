<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Crypt;
class UsersUndanganController extends Controller
{
    public function index()
    {
        $users = \App\Users::all();
 
        return $users->toJson();
    }
    public function daftar(Request $request){
        $originalDate = $request->tgl_lahir;
        $newDate = date("Y-m-d", strtotime($originalDate));
        $getId = DB::table('tb_undangan_user')->where('email', $request->email)->select('id')->first();
        $insert = DB::table('tb_undangan_user')->where('email', $request->email)->update([
            'nama' => $request->nama,
            'tgl_lahir' => $newDate,
            'jk' => $request->jk,
        ]);
        $insertAA = DB::table('tb_undangan')->insert([
            'id_user' => $getId->id,
            'kode_reg' => 'HUNT'.Str::random(5),
            'is_input' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);
        $msg = [
            'success' => true,
            'message' => 'Daftar Event successfully!'
        ];
 
        return response()->json($msg);
    }
    public function store(Request $request)
    {
        $validatedData = $request->validate([
          'email' => 'required|email',
        ]);

        $getId=DB::table('tb_undangan_user')->where('email', $validatedData['email'])->select('id')->first();
 
        $project = \App\Users::create([
          'nama' => '',
          'email' => $validatedData['email'],
          'link' => 'http://localhost:8000/user/event/register/'.base64_encode($validatedData['email'])
        ]);
        
        $msg = [
            'success' => true,
            'message' => 'Users created successfully!'
        ];
 
        return response()->json($msg);
    }

    public function getListDesigner($id){
        $get=DB::table('tb_userfav_design')->join('tb_designer','tb_userfav_design.id_designer','=','tb_designer.id')->where('tb_userfav_design.id_user', $id)->get();
        $msg = [
            'data' => $get,
            'message' => 'Users created successfully!'
        ];
 
        return response()->json($msg);

    }

    public function getMl($ml){
        $users = DB::table('tb_undangan_user')->where('email',$ml)->first();
        $msg = [
            'data' => $users,
            'message' => 'successfully'
        ];
 
        return response()->json($msg);
    }

    public function setDesigner(Request $request){
        $getId = DB::table('tb_undangan_user')->where('email', $request->email)->first();

        $no=0;
        for($c=0; $c<count($request->c); $c++){
            DB::table('tb_userfav_design')->insert([
                'id_user' => $getId->id,
                'id_designer' => $request->c[$c],
            ]);
        }
        $msg = [
            'message' => 'successfully'
        ];
        return response()->json($msg);
        
    }
    public function designer(){
        $get=DB::table('tb_designer')->get();
        $d = array();
        $no=0;
        foreach($get as $getA){
           $getFoto = DB::table('tb_designer_img')->where('id_designer', $getA->id) ->get();
           $d[$no]['designer'] = $getA->nama;
           $d[$no]['id'] = $getA->id;
           foreach($getFoto as $gfEach){
            $d[$no]['foto'][] = $gfEach->image;
           }

           $no++;
        }
        $msg = [
            'data' => $d,
            'message' => 'successfully'
        ];
        return response()->json($msg);
    }

    public function cekR($ml){
        $getId = DB::table('tb_undangan_user')->where('email',$ml)->first();
        $users = DB::table('tb_undangan')->where('id_user',$getId->id)->where('is_input', 1)->count();
        $msg = [
            'data' => $users,
            'message' => 'successfully'
        ];
 
        return response()->json($msg);
    }
    public function getR($id){
        $users = DB::table('tb_undangan')->join('tb_undangan_user','tb_undangan.id_user','=','tb_undangan_user.id')->where('tb_undangan.id_user',$id)->where('is_input', 1)->first();
        $msg = [
            'data' => $users,
            'message' => 'successfully'
        ];
 
        return response()->json($msg);
    }

    public function sendLink($id){
        $user = DB::table('tb_undangan_user')->where('id', $id)->first();
        $to_name = $user->email;
        $to_email = $user->email;
        $data = array('name'=>$user->email, "body" => "Halo, berikut Kami sertakan link untuk pendaftaran event HUNTBAZAAR, Klik Link ini untuk daftar ".$user->link);
        
        Mail::send('emails.mail', $data, function($message) use ($to_name, $to_email) {
            $message->to($to_email, $to_name)
            ->subject('Link Registrasi Event HUNTBAZAAR');
            $message->from('mkbakhtiar.new@gmail.com','Link pendaftaran event HUNTBAZAAR');
        });
    }
    public function getUsers($id) // for edit and show
    {
        $article = \App\Users::find($id);
 
        return $article->toJson();
    }
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
          'email' => 'required|email',
        ]);
 
        $article = \App\Users::find($id);
        $article->email = $validatedData['email'];
        $article->link = 'http://localhost:8000/user/event/register/'.base64_encode($validatedData['email']);
        $article->save();
 
        $msg = [
            'success' => true,
            'message' => 'Admin updated successfully'
        ];
 
        return response()->json($msg);
    }
    public function delete($id)
    {
        $admin = \App\Users::find($id);
        if(!empty($admin)){
            $admin->delete();
            $msg = [
                'success' => true,
                'message' => 'Users deleted successfully!'
            ];
            return response()->json($msg);
        } else {
            $msg = [
                'success' => false,
                'message' => 'Users deleted failed!'
            ];
            return response()->json($msg);
        }
    }
}
