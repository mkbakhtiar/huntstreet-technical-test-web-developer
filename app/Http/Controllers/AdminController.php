<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AdminController extends Controller
{
    public function index()
    {
        $admins = \App\Admin::all();
 
        return $admins->toJson();
    }
    public function store(Request $request)
    {
        $validatedData = $request->validate([
          'nama' => 'required',
          'username' => 'required',
          'password' => 'required',
        ]);
 
        $project = \App\Admin::create([
          'nama' => $validatedData['nama'],
          'username' => $validatedData['username'],
          'password' => Hash::make($validatedData['password']),
        ]);
 
        $msg = [
            'success' => true,
            'message' => 'Admin created successfully!'
        ];
 
        return response()->json($msg);
    }

    public function getAdmin($id) // for edit and show
    {
        $article = \App\Admin::find($id);
 
        return $article->toJson();
    }

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'username' => 'required|email',
            'password' => 'required',
        ]);
        if($validator->fails()){
            return response()->json($validator->messages(), 401);
        }
        $admin = DB::table('tb_admin')->where('username', $request->username)->first();
        $check = Hash::check($request->password, $admin->password);
        if($check){
            return response()->json([
                'msg' => 'You are logged in',
            ]);
        }else{
            return response()->json([
                'msg' => 'Incorrect login details'.$check,
            ], 401);
        }
    }

    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
          'nama' => 'required',
          'username' => 'required',
          'password' => 'required',
        ]);
 
        $article = \App\Admin::find($id);
        $article->nama = $validatedData['nama'];
        $article->username = $validatedData['username'];
        $article->password = $validatedData['password'];
        $article->save();
 
        $msg = [
            'success' => true,
            'message' => 'Admin updated successfully'
        ];
 
        return response()->json($msg);
    }
    public function delete($id)
    {
        $admin = \App\Admin::find($id);
        if(!empty($admin)){
            $admin->delete();
            $msg = [
                'success' => true,
                'message' => 'Admin deleted successfully!'
            ];
            return response()->json($msg);
        } else {
            $msg = [
                'success' => false,
                'message' => 'Admin deleted failed!'
            ];
            return response()->json($msg);
        }
    }
}
