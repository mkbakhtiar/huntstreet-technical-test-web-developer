<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;

class DesignerFotoController extends Controller
{
    public function index(){
        $getData = DB::table('tb_designer')->join('tb_designer_foto','tb_designer.id','=','tb_designer_foto.id_designer')->get();
        return $getData->toJson();
    }
    public function show($id){
        $getData = DB::table('tb_designer_img')
        ->select('tb_designer_img.image','tb_designer_img.id as id_foto','tb_designer.nama')
        ->join('tb_designer','tb_designer_img.id_designer','=','tb_designer.id')
        ->where('tb_designer_img.id_designer', $id)
        ->get();
        return $getData->toJson();
    }

    public function designerName($id){
        $get = DB::table('tb_designer')->where('id', $id)->first();
        $msg = [
            'data' => $get->nama,
        ];
        return response()->json($msg);
    }
    public function store(Request $request)
    {
        if($request->get('image')){
          $image = $request->get('image');
          $name = time().'.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
          \Image::make($request->get('image'))->save(public_path('images/').$name);
        }
        $update = DB::table('tb_designer_img')->insert(['id_designer' => $request->id_designer, 'image' => $name]);
        return response()->json(['success' => 'You have successfully uploaded an image'], 200);
        $request->validate([
            'file' => 'required|mimes:jpg,jpeg,png,csv,txt,xlx,xls,pdf|max:2048'
        ]);
     }
     public function delete($id){
        $designer = \App\DesignerFoto::find($id);
        if(!empty($designer)){
            $designer->delete();
            $msg = [
                'success' => true,
                'message' => 'Designer Foto deleted successfully!'
            ];
            return response()->json($msg);
        } else {
            $msg = [
                'success' => false,
                'message' => 'Designer Foto deleted failed!'
            ];
            return response()->json($msg);
        }
    }
}
