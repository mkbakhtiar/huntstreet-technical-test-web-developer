require('./bootstrap');
 
window.Vue = require('vue');
 
import VueRouter from 'vue-router';
import VueAxios from 'vue-axios';
import axios from 'axios';
import App from './layouts/App.vue'
import VueSweetalert2 from 'vue-sweetalert2'; 
// If you don't need the styles, do not connect
import 'sweetalert2/dist/sweetalert2.min.css';
 
Vue.use(VueRouter);
Vue.use(VueSweetalert2);
Vue.use(VueAxios, axios);
 
import AdminIndex from './components/Admin/AdminIndex.vue';
import AdminCreate from './components/Admin/AdminCreate.vue';
import AdminShow from './components/Admin/AdminShow.vue';
import AdminEdit from './components/Admin/AdminEdit.vue';
import Login from './components/Login.vue';

// Users View Vue
import UsersIndex from './components/Users/UsersIndex.vue';
import UsersCreate from './components/Users/UsersCreate.vue';
import UsersShow from './components/Users/UsersShow.vue';
import UsersEdit from './components/Users/UsersEdit.vue';

import DesignerIndex from './components/Designer/DesignerIndex.vue';
import DesignerCreate from './components/Designer/DesignerCreate.vue';
import DesignerEdit from './components/Designer/DesignerEdit.vue';
import DesignerFoto from './components/Designer/DesignerFoto.vue';
import EventRegister from './components/Event/Register.vue';
import EventHome from './components/Event/Home.vue';
 
const routes = [
  {
      name: 'login',
      path: '/login',
      component: Login
  },
  {
      name: 'admin_home',
      path: '/',
      component: AdminIndex
  },
  {
      name: 'admin_create',
      path: '/admin/create',
      component: AdminCreate
  },
  {
      name: 'admin_edit',
      path: '/admin/edit/:id',
      component: AdminEdit
  },
  {
    name: 'admin_show',
    path: '/admin/show/:id',
    component: AdminShow
    },
    {
        name: 'users_home',
        path: '/users',
        component: UsersIndex
    },
    {
        name: 'users_create',
        path: '/users/create',
        component: UsersCreate
    },
    {
        name: 'users_edit',
        path: '/users/edit/:id',
        component: UsersEdit
    },
    {
      name: 'users_show',
      path: '/users/show/:id',
      component: UsersShow
  },
    {
      name: 'designer_home',
      path: '/designer',
      component: DesignerIndex
  },
    {
      name: 'designer_create',
      path: '/designer/create',
      component: DesignerCreate
  },
    {
      name: 'designer_edit',
      path: '/designer/edit',
      component: DesignerEdit
  },
    {
      name: 'designer_foto',
      path: '/designer/foto/:id',
      component: DesignerFoto
  },
    {
      name: 'event_register',
      path: '/user/event/register/:link',
      component: EventRegister
  },
    {
      name: 'event_home',
      path: '/user/event/home/:id',
      component: EventHome
  },
];
// Vue.component('file-upload-component', require('./components/Designer/DesignerFoto.vue').default);

const router = new VueRouter({ mode: 'history', routes: routes});
const app = new Vue(Vue.util.extend({ router }, App)).$mount('#app');