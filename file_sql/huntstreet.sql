/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50724
 Source Host           : localhost:3306
 Source Schema         : huntstreet

 Target Server Type    : MySQL
 Target Server Version : 50724
 File Encoding         : 65001

 Date: 18/09/2021 19:12:48
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for failed_jobs
-- ----------------------------
DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE `failed_jobs`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `connection` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES (1, '2014_10_12_000000_create_users_table', 1);
INSERT INTO `migrations` VALUES (2, '2019_08_19_000000_create_failed_jobs_table', 1);
INSERT INTO `migrations` VALUES (3, '2021_09_15_141751_create_admin_table', 1);
INSERT INTO `migrations` VALUES (4, '2021_09_15_142248_create_tb_undangan_user_table', 1);
INSERT INTO `migrations` VALUES (5, '2021_09_15_142305_create_tb_undangan_table', 1);
INSERT INTO `migrations` VALUES (6, '2021_09_15_142322_create_tb_settings_table', 1);
INSERT INTO `migrations` VALUES (7, '2021_09_15_142337_create_tb_designer_table', 1);
INSERT INTO `migrations` VALUES (8, '2021_09_15_142352_create_tb_designer_img_table', 1);
INSERT INTO `migrations` VALUES (9, '2021_09_15_143143_create_tb_userfav_design_table', 1);

-- ----------------------------
-- Table structure for tb_admin
-- ----------------------------
DROP TABLE IF EXISTS `tb_admin`;
CREATE TABLE `tb_admin`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nama` char(75) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` char(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` char(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_log` datetime(0) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_admin
-- ----------------------------
INSERT INTO `tb_admin` VALUES (1, 'Admin', 'admin@gmail.com', '$2y$10$q/DwXfTQEZ1Im8zcNNh5texyuDAdyGmjGK5Wl/zuHHQPZbpGjth7O', NULL, '2021-09-15 22:04:34', NULL);

-- ----------------------------
-- Table structure for tb_designer
-- ----------------------------
DROP TABLE IF EXISTS `tb_designer`;
CREATE TABLE `tb_designer`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nama` char(75) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_designer
-- ----------------------------
INSERT INTO `tb_designer` VALUES (2, 'AAPE by A Bathing Ape', NULL, NULL);
INSERT INTO `tb_designer` VALUES (3, 'a.testoni', NULL, NULL);
INSERT INTO `tb_designer` VALUES (4, '7 FOR ALL MANKIND', NULL, NULL);

-- ----------------------------
-- Table structure for tb_designer_img
-- ----------------------------
DROP TABLE IF EXISTS `tb_designer_img`;
CREATE TABLE `tb_designer_img`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_designer` int(10) UNSIGNED NOT NULL,
  `image` char(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_designer_img
-- ----------------------------
INSERT INTO `tb_designer_img` VALUES (3, 2, '1631948584.jpeg', NULL, NULL);
INSERT INTO `tb_designer_img` VALUES (4, 2, '1631948591.jpeg', NULL, NULL);
INSERT INTO `tb_designer_img` VALUES (5, 2, '1631948597.jpeg', NULL, NULL);
INSERT INTO `tb_designer_img` VALUES (6, 3, '1631949143.jpeg', NULL, NULL);
INSERT INTO `tb_designer_img` VALUES (7, 3, '1631949149.jpeg', NULL, NULL);
INSERT INTO `tb_designer_img` VALUES (8, 3, '1631949155.jpeg', NULL, NULL);
INSERT INTO `tb_designer_img` VALUES (9, 4, '1631949243.jpeg', NULL, NULL);
INSERT INTO `tb_designer_img` VALUES (10, 4, '1631949248.jpeg', NULL, NULL);
INSERT INTO `tb_designer_img` VALUES (11, 4, '1631949254.jpeg', NULL, NULL);

-- ----------------------------
-- Table structure for tb_settings
-- ----------------------------
DROP TABLE IF EXISTS `tb_settings`;
CREATE TABLE `tb_settings`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `exp_registrai` datetime(0) NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_settings
-- ----------------------------
INSERT INTO `tb_settings` VALUES (1, '2021-10-15 00:00:00', NULL, NULL);

-- ----------------------------
-- Table structure for tb_undangan
-- ----------------------------
DROP TABLE IF EXISTS `tb_undangan`;
CREATE TABLE `tb_undangan`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_user` int(10) UNSIGNED NOT NULL,
  `kode_reg` char(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_input` int(10) UNSIGNED NOT NULL,
  `is_sendmail` int(10) UNSIGNED NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_undangan
-- ----------------------------
INSERT INTO `tb_undangan` VALUES (11, 4, 'HUNTaA1In', 1, NULL, '2021-09-18 19:00:43', NULL);

-- ----------------------------
-- Table structure for tb_undangan_user
-- ----------------------------
DROP TABLE IF EXISTS `tb_undangan_user`;
CREATE TABLE `tb_undangan_user`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nama` char(75) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `tgl_lahir` date NULL DEFAULT NULL,
  `jk` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `email` char(35) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` char(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `input_by` int(10) UNSIGNED NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_undangan_user
-- ----------------------------
INSERT INTO `tb_undangan_user` VALUES (4, 'Bakhtiar', '2021-09-18', 'L', 'mkbakhtiar.new@gmail.com', 'http://localhost:8000/user/event/register/bWtiYWtodGlhci5uZXdAZ21haWwuY29t', NULL, '2021-09-17 01:24:29', '2021-09-18 14:55:26');

-- ----------------------------
-- Table structure for tb_userfav_design
-- ----------------------------
DROP TABLE IF EXISTS `tb_userfav_design`;
CREATE TABLE `tb_userfav_design`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_user` int(10) UNSIGNED NOT NULL,
  `id_designer` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_userfav_design
-- ----------------------------
INSERT INTO `tb_userfav_design` VALUES (13, 4, 2, NULL, NULL);
INSERT INTO `tb_userfav_design` VALUES (14, 4, 3, NULL, NULL);

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp(0) NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `users_email_unique`(`email`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
